<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ 'Prekių katalogas' }}</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <style>
        html, body {
            background-color: #eee;
            font-family: calibri, sans-serif;
        }

        #app {
            width: 760px;
            margin: 20px auto;
        }
        .box {
            width: 230px;
            background-color: #fff;
            box-shadow: 0 0 20px rgba(0, 0, 0, 0.1);
            display: inline-block;
            margin: 0 10px;
            position: relative;
        }
        img {
            width: 230px;
        }
        h2 {
            margin-left: 20px;
        }
        p {
            margin-left: 20px;
        }
        #cart {
            margin-top: 50px;
            overflow: hidden;
        }
        #head {
            width: 100%;
            border-bottom: 1px solid #BFBFBF;
            height: 40px;
            display: block;
        }
        h3 {
            display: inline-block;
            line-height: 40px;
            margin: 0;
        }
        #price {
            display: inline-block;
            color: #777777;
            margin-left: 200px;
            line-height: 40px;
        }
        #quantity {
            display: inline-block;
            color: #777777;
            margin-left: 100px;
            line-height: 40px;
        }
        #total {
            display: inline-block;
            color: #777777;
            line-height: 40px;
            float: right;
        }
        .row {
            width: 100%;
            border-bottom: 1px solid #BFBFBF;
            overflow: hidden;
            padding: 10px 0;
            display: block;
            flaot: left;
        }
        img {
            height: 100px;
        }
        h4 {
            float: left;
            margin: 0;
            line-height: 100px;
            margin-left: 20px;
            width: 100px;
        }
        p {
            float: left;
            margin: 0;
            width: 80px;
            line-height: 100px;
            margin-left: 35px;
            text-align: center;
        }
        .qty-minus {
            float: left;
            width: 20px;
            line-height: 100px;
            margin-left: 60px;
            text-align: center;
            cursor: pointer;
        }
        .qty {
            float: left;
            width: 20px;
            line-height: 100px;
            margin-left: 20px;
            text-align: center;
        }
        .qty-plus {
            float: left;
            width: 20px;
            line-height: 100px;
            margin-left: 20px;
            text-align: center;
            cursor: pointer;
        }
        .del {
            float: left;
            width: 80px;
            line-height: 100px;
            margin-left: 60px;
            cursor: pointer;
            text-decoration: underline;
            color: #ED277F;
        }
        .totalprice {
            float: left;
            width: 80px;
            line-height: 100px;
            margin-left: 10px;
            text-align: right;
        }

        h5 {
            font-size: 1.2rem;
            text-align : right;
        }

    </style>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.1.10/vue.min.js"></script>
</head>
<body>
<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <button type="button" class="btn btn-primary">Pienas</button>
                    <button type="button" class="btn btn-primary">Duona</button>
                    <button type="button" class="btn btn-primary">Visi</button>
                </li>
            </ul>
        </div>
    </div>
</nav>

<template id="whislist">
<button type="button" class="btn btn-primary">Whislistas @{{whislist()}}</button>
</template>
<!-- Page Header -->
<header class="masthead">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
                <div class="site-heading">
                    <h1>Shopping cart'as</h1>
                    <span class="subheading">Haha :)))</span>
                </div>
            </div>
        </div>
    </div>
</header>
<div id="app">
    <div id="product">
        <item v-for="item in items" v-bind:item_data="item"></item>
    </div>
    <div id="cart">
        <div id="head">
            <h3>Krepšelis</h3>
            <div id="price">Kaina</div>
            <div id="quantity">Kiekis</div>
            <div id="total">Viso</div>
        </div>
        <buyitem v-for="buyitem in buyitems" v-bind:buy_data="buyitem"></buyitem>
        <h5 v-if="total()">Suma: @{{total()}} EUR</h5>
    </div>
</div>


<template id="product-box">
    <div class="box" style="margin-bottom: 50px">
        <img :src="item_data.img"/>
        <a href="#"><img src="{{ asset("images/add_new.png") }}" v-on:click="addItem(item_data)" style="position: relative; width: 50px; height: 50px;"></a>
        <a href="#"><img src="{{ asset("images/favorite.png") }}" v-on:click="whislist += 1"  style="position: relative; width: 50px; height: 50px;"></a>
        <h2>@{{item_data.title}}</h2>
        <p>@{{item_data.price}} EUR</p>
        @{{whislist}}
    </div>
</template>
<template id="buy-box">

    <div class="row">
        <img :src="buy_data.img" style="width: 50px; height: 50px;"/>
        <h4>@{{buy_data.title}}</h4>
        <p> @{{buy_data.price}} EUR</p>
        <div class="qty-minus" v-on:click="minusQty(buy_data)">-</div>
        <div class="qty">@{{buy_data.qty}}</div>
        <div class="qty-plus" v-on:click="plusQty(buy_data)">+</div>
        <div class="del" v-on:click="removeItem(buy_data)">Ištrinti</div>
        <div class="totalprice">@{{buy_data.total}}</div>
    </div>
</template>
<main>
    @yield('content')
</main>
<!-- Footer -->
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-10 mx-auto">
2019 &copy; Kęstas Daukantas 2019
            </div>
        </div>
    </div>
</footer>
<script>
    var varskesClick = 0;
    var surisClick = 0;
    var pienasClick = 0;
    var dvaroClick = 0;
    var jogurtasClick = 0;
    var palangaClick = 0;
    var whislist = 0;
    Vue.component("item", {
        template: "#product-box",
        props: ["item_data", "buyitems"],
        methods: {
            addItem: function(item_data) {
                if (item_data.id == "varskes") {
                    varskesClick += 1;
                    if (varskesClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "varskes");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total = this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                        console.log(i);
                    }
                } else if (item_data.id == "suris") {
                    surisClick += 1;
                    if (surisClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "suris");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total =this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                } else if (item_data.id == "pienas") {
                    pienasClick += 1;
                    if (pienasClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "pienas");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total =this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                } else if (item_data.id == "dvaro") {
                    dvaroClick += 1;
                    if (dvaroClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "dvaro");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total =this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                } else if (item_data.id == "jogurtas") {
                    jogurtasClick += 1;
                    if (jogurtasClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "jogurtas");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total =this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                } else if (item_data.id == "palanga") {
                    palangaClick += 1;
                    if (palangaClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "palanga");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total =this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                } else {
                    palangaClick += 1;
                    if (palangaClick <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "palanga");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total = this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                }
                console.log(varskesClick, surisClick, pienasClick, dvaroClick, jogurtasClick, palangaClick);
            },
            addItemonWishList: function(item_data) {
                if (item_data.id == "palanga") {
                    whislist += 1;
                    if (whislist <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "palanga");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total = this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                        console.log(i);
                    }
                } else if (item_data.id == "eco-bag") {
                    whislist += 1;
                    if (whislist <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "varskes");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total =this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                } else {
                    whislist += 1;
                    if (whislist <= 1) {
                        this.pushData();
                    } else {
                        var i = this.findIndex(this.$parent.buyitems, "id", "suris");
                        this.$parent.buyitems[i].qty += 1;
                        this.$parent.buyitems[i].total = this.$parent.buyitems[i].qty*this.$parent.buyitems[i].price;
                    }
                }
            },
            pushData: function() {
                this.$parent.buyitems.push({
                    img: this.item_data.img,
                    title: this.item_data.title,
                    price: this.item_data.price,
                    qty: 1,
                    total: this.item_data.price,
                    id: this.item_data.id
                });
            },
            findIndex: function(array, attr, value) {
                for (var i = 0; i < array.length; i += 1) {
                    if (array[i][attr] === value) {
                        return i;
                    }
                }
                return -1;
            },
        }
    });
    Vue.component("buyitem", {
        template: "#buy-box",
        props: ["buy_data", "buyitems"],
        methods: {
            removeItem: function(buy_data) {
                var index = this.$parent.buyitems.indexOf(buy_data);
                this.$parent.buyitems.splice(index, 1);
                if (buy_data.id == "varskes") {
                    varskesClick = 0;
                } else if (buy_data.id == "suris") {
                    surisClick = 0;
                } else if (buy_data.id == "pienas") {
                    pienasClick = 0;
                } else if (buy_data.id == "dvaro") {
                    dvaroClick = 0;
                }  else if (buy_data.id == "jogurtas") {
                    jogurtasClick = 0;
                }
                else {
                    palangaClick = 0;
                }
            },
            plusQty: function(buy_data){
                buy_data.qty += 1;
                buy_data.total = Number((buy_data.qty*buy_data.price).toFixed(2));
            },
            minusQty: function(buy_data) {
                buy_data.qty -= 1;
                if (buy_data.qty <= 0) {
                    var index = this.$parent.buyitems.indexOf(buy_data);
                    this.$parent.buyitems.splice(index, 1);
                    if (buy_data.id == "varskes") {
                        varskesClick = 0;
                    } else if (buy_data.id == "suris") {
                        surisClick = 0;
                    } else if (buy_data.id == "pienas") {
                        pienasClick = 0;
                    } else if (buy_data.id == "dvaro") {
                        dvaroClick = 0;
                    }  else if (buy_data.id == "jogurtas") {
                        jogurtasClick = 0;
                    }
                    else {
                        palangaClick = 0;
                    }
                }
                else {
                    buy_data.total = buy_data.qty * buy_data.price;
                }
            }

        }
    });

    var app = new Vue({
        el: "#app",
        data: {
            items: [
                {
                    img: "http://www.ve.lt/uploads/img/catalog/1/925/758/varskes-surelis-magija-populiariausias-jau-ketvirtus-metus2.jpg",
                    title: "Varškės sūrelis",
                    price: "1.47",
                    id: "varskes"
                },
                {
                    img: "https://www.barbora.lt/api/Images/GetInventoryImage?id=d13524fe-2ee1-445f-affb-2b5cf752fcb5",
                    title: "Rokiškio sūris",
                    price: "1.52",
                    id: "suris"
                },
                {
                    img: "https://pienozvaigzdes.lt/194-large_default/dvaro-pienas-25-riebumo-1l.jpg",
                    title: "Pieno pienas",
                    price: "1.64",
                    id: "pienas"
                },
                {
                    img: "https://pienozvaigzdes.lt/194-large_default/dvaro-pienas-25-riebumo-1l.jpg",
                    title: "Dvaro",
                    price: "1.81",
                    id: "dvaro"
                },
                {
                    img: "https://i.ytimg.com/vi/b6tkw-UtXRs/maxresdefault.jpg",
                    title: "jogurtas",
                    price: "1.84",
                    id: "jogurtas"
                },
                {
                    img: "http://www.duona.com/uploads/Products/product_81/10_palanga-mociuciu_su-saulegrazomis_pusele_1473361735.png",
                    title: "Palanga",
                    price: "2.23",
                    id: "palanga"
                },

            ],
            buyitems: []
        },
        methods: {
            total: function(){
                var sum = 0;
                this.buyitems.forEach(function(buyitem){
                    sum += parseFloat(buyitem.total);
                });
                return Number((sum).toFixed(2));
            }
        }
    });

</script>
</body>
</html>