-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2019 m. Bir 07 d. 08:42
-- Server version: 10.1.26-MariaDB
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `prekiu_kat`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `kategorijos`
--

CREATE TABLE `kategorijos` (
  `id` int(11) NOT NULL,
  `pavadinimas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `kategorijos`
--

INSERT INTO `kategorijos` (`id`, `pavadinimas`) VALUES
(1, 'pienas'),
(2, 'duona');

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_06_06_140057_create_kategorijos_table', 1),
(2, '2019_06_06_140311_create_prekes_table', 1);

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `prekes`
--

CREATE TABLE `prekes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `kategorijos_id` int(11) NOT NULL,
  `pavadinimas` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kaina` float NOT NULL,
  `aprasymas` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nuotrauka` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Sukurta duomenų kopija lentelei `prekes`
--

INSERT INTO `prekes` (`id`, `kategorijos_id`, `pavadinimas`, `kaina`, `aprasymas`, `nuotrauka`) VALUES
(2, 1, 'Varškės sūrelis', 1.47, NULL, 'http://www.ve.lt/uploads/img/catalog/1/925/758/varskes-surelis-magija-populiariausias-jau-ketvirtus-metus2.jpg'),
(3, 1, 'Rokiškio pienas', 1.52, NULL, 'https://www.barbora.lt/api/Images/GetInventoryImage?id=d13524fe-2ee1-445f-affb-2b5cf752fcb5'),
(4, 1, 'Pieno žvaigždės pienas', 1.64, NULL, 'https://pienozvaigzdes.lt/194-large_default/dvaro-pienas-25-riebumo-1l.jpg'),
(5, 1, 'Vilkyškių varškės sūris', 1.81, NULL, 'http://www.vilkyskiu.lt/wp-content/uploads/2012/02/varskes-suris1-327x212.png'),
(7, 1, 'Vilkyškių jogurto gėrimas', 1.84, NULL, 'https://i.ytimg.com/vi/b6tkw-UtXRs/maxresdefault.jpg'),
(8, 2, 'Duona Palanga močiučių', 2.23, NULL, 'http://www.duona.com/uploads/Products/product_81/10_palanga-mociuciu_su-saulegrazomis_pusele_1473361735.png'),
(9, 2, 'Klaipėdos plikyta duona', 2.11, NULL, 'http://www.duona.com/uploads/Products/product_79/4_klaipedos-plikyta-su-saulegrazomis_pusele_1473361535.png'),
(11, 2, 'Klaipėdos raikyta duona', 2.31, NULL, 'http://www.duona.com/uploads/_CGSmartImage/6_klaipedos-plikyta-su-daigintais-kvieciais_kepalas_1467115138-9d235a9cfb5167027f5f06425101f8c4.png'),
(12, 2, 'Klaipėdos batonas', 1.54, NULL, 'http://kasuvalgyti.lt/wp-content/uploads/2009/11/IMGP0734_2.jpg'),
(13, 2, 'Bočių duona', 1.52, NULL, 'http://www.gpoint7.com/content/images/thumbs/0001639_bread-klaipedos-duona-bociu-700g-case20_300.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kategorijos`
--
ALTER TABLE `kategorijos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prekes`
--
ALTER TABLE `prekes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kategorijos_id` (`kategorijos_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kategorijos`
--
ALTER TABLE `kategorijos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prekes`
--
ALTER TABLE `prekes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Apribojimai eksportuotom lentelėm
--

--
-- Apribojimai lentelei `prekes`
--
ALTER TABLE `prekes`
  ADD CONSTRAINT `prekes_ibfk_1` FOREIGN KEY (`kategorijos_id`) REFERENCES `kategorijos` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
